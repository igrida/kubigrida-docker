#!/bin/bash

rmdir /usr/lib/cgi-bin
ln -s /usr/local/lib/cgi-bin /usr/lib/

a2enmod cgi

source /etc/apache2/envvars
exec apache2 -D FOREGROUND